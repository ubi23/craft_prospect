﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SatelliteManager : MonoBehaviour
{

    public DataManager dataManager;

    private float lasttimer = 0;
    private float durations = 0;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(callEverySecond());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator callEverySecond()
    {
        while (true)
        {
            float compiletime = 0;
            try
            {
                compiletime = continueIterating();
            }
            catch( Exception e )
            {
                
            }
            yield return new WaitForSeconds(durations - compiletime);
        }
    }


    float continueIterating()
    {
        float time = Time.time;
        dataManager.iterate();
        print(dataManager.getCurrentTime());
        print("Current x position is " + dataManager.getCurrentPosition().x);
        print("----------------------------------");

        durations = dataManager.getCurrentTime() - lasttimer;
        lasttimer = dataManager.getCurrentTime();
        return Time.time - time;
    }
}
