﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class DataManager : MonoBehaviour {

    public string csv_path;
    private List<SatellitePosition> satellitesPosition;
    private int iterator = 0;

	// Use this for initialization
    void Start () {
        print("Warning: \n\t The measurement is 0.1 of the real value. And they are in KM matric. For example the radius of the earth is 637.8 KM");
        this.satellitesPosition = new List<SatellitePosition>();
        readCSV();
        print("Reading completted.");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void readCSV()
    {
        string fileData = System.IO.File.ReadAllText(csv_path);
        string[] lines = fileData.Split('\n');
        for (int i = 0; i < lines.Length; i++)
        {
            string[] lineData = (lines[i].Trim()).Split(',');
            try
            {
                //print(float.Parse(lineData[1]));
                SatellitePosition satellite = new SatellitePosition(float.Parse(lineData[1]));
                satellite.setPosition(float.Parse(lineData[2]), float.Parse(lineData[3]), float.Parse(lineData[4]));
                satellitesPosition.Add(satellite);
            }
            catch(Exception e)
            {
                print(e);
            }
        }
    }

    public Vector3 getCurrentPosition()
    {
        return this.satellitesPosition[this.iterator].getPosition();
    }

    public float getCurrentTime()
    {
        return this.satellitesPosition[this.iterator].getTime();
    }

    public bool iterate()
    {
        if (iterator == this.satellitesPosition.Count)
        {
            return false;
        }
        iterator += 1;
        return true;
    }
}


public class SatellitePosition
{
    private float time;
    private Vector3 position;

    public SatellitePosition(float ctime){
        this.time = ctime;
    }

    public void setPosition(float x, float y, float z)
    {
        position = new Vector3(x,y,z);
    }

    public Vector3 getPosition()
    {
        return position;
    }

    public float getTime()
    {
        return time;
    }
}
